var loginPage = function () {

    var pageElements = require('../pageElements/pageElements');

    this.startPage = 'http://localhost:9000/#/login';

    this.fillLoginAndPass = function (login,password) {
        pageElements.loginPageElements.loginField.sendKeys(login);
        pageElements.loginPageElements.passwordField.sendKeys(password);
    };

    this.check = function (value1, value2) {
        expect(value1).toEqual(value2);
    }
};

module.exports = new loginPage();