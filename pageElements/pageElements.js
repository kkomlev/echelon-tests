var pageElements = function () {

    this.getElement = function (method, a) {
        switch (method) {
            case 'css':
                return element(by.css(a));
                break;
            case 'model':
                return element(by.model(a));
                break;
            case 'binding':
                return element(by.binding(a));
                break;
            case  'id': {
                return element(by.id(a));
            }
            default: {
                throw console.log("wrong name of method - " + method)
            }
        }
    };

    this.loginPageElements = {

        loginField: this.getElement("model", "loginCtrl.user.name"),

        passwordField: this.getElement("model", "loginCtrl.user.password"),

        rememberCheckBox: this.getElement("css", ".login-form .remember-me-block md-checkbox"),

        forgotButton: this.getElement("css", ".login-form .forgot-password"),

        loginButton: this.getElement("css", "md-actions button"),

        forgotEmailField: this.getElement("css", "body > div.md-dialog-container.ng-scope > md-dialog > form > md-dialog-content > md-input-container > input"),

        cancelButton: this.getElement("css", "body > div.md-dialog-container.ng-scope > md-dialog > form > md-dialog-actions > button:nth-child(1)"),

        resetPasswordButton: this.getElement("css", "body > div.md-dialog-container.ng-scope > md-dialog > form > md-dialog-actions > button.md-primary.md-raised.md-button.ng-scope.md-ink-ripple")
    };

    this.dashboardPageElements = {

        editButton: this.getElement("css", "body > div > section > div > md-content > div > div > button > md-icon"),

        plusButton: this.getElement("css", "body > div > section > div > md-content > div > div > button.md-icon-button.md-button.ng-scope.md-ink-ripple > md-icon"),

        toolbarWidgets: this.getElement("css","md-list md-list-item")
    };
};
module.exports = new pageElements();