describe('test app', function () {
    var until = protractor.ExpectedConditions;
    var login_page = require('../functions/login_page.js');
    var pageElements = require('../pageElements/pageElements');
    beforeEach(function () {
        browser.get(login_page.startPage);

    });

    it("testing valid url", function () {
        login_page.check(browser.getCurrentUrl(), login_page.startPage)
    });

    describe("testing login page", function () {

        it('login without data', function () {
            pageElements.loginPageElements.loginButton.click();
        });

        it('login with valid data', function () {

            login_page.fillLoginAndPass("admin", "admin");
            pageElements.loginPageElements.rememberCheckBox.click();
            pageElements.loginPageElements.loginButton.click();
        });

        it('login with invalid data', function () {
            login_page.fillLoginAndPass("admin11223344---------", "admin11223344---------");
            pageElements.loginPageElements.rememberCheckBox.click();
            pageElements.loginPageElements.loginButton.click();
        });

        describe("testing forgot password", function () {

            it("enter forgot pass dialog and cancel it", function () {
                login_page.fillLoginAndPass("admin11223344---------", "admin11223344---------");
                pageElements.loginPageElements.forgotButton.click();
                pageElements.loginPageElements.cancelButton.click();
            });

            it("trying to reset password", function () {
                login_page.fillLoginAndPass("admin", "admin");
                pageElements.loginPageElements.rememberCheckBox.click();
                pageElements.loginPageElements.forgotButton.click();
                console.log(pageElements.loginPageElements.forgotEmailField.name);
                browser.wait(until.presenceOf(pageElements.loginPageElements.forgotEmailField), 5000,' not located in the DOM');
                pageElements.loginPageElements.forgotEmailField.sendKeys("kakaka@maka.er");
                console.log(pageElements.loginPageElements.forgotEmailField.name);
                pageElements.loginPageElements.resetPasswordButton.click();
                pageElements.loginPageElements.cancelButton.click();
            });
        });
        describe("testing what i dont know", function () {
            it('testet', function () {
                element.all(by.css('input')).sendKeys("admin");
                pageElements.loginPageElements.rememberCheckBox.click();
                pageElements.loginPageElements.loginButton.click();
                pageElements.dashboardPageElements.editButton.click();
                pageElements.dashboardPageElements.plusButton.click();
                element.all(by.css('md-list md-list-item')).click();
            });
        });

    })


});